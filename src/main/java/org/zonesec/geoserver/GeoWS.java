package org.zonesec.geoserver;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.zonesec.geoserver.api.GenerateSensorGeoJson;

//@RequestParam(value="name", defaultValue="World") String name
@RestController
public class GeoWS {

    private final AtomicLong counter = new AtomicLong();

    @RequestMapping("/sensorGeoJson")
    public String greeting(
            ) {
        GenerateSensorGeoJson pj=new GenerateSensorGeoJson();
        return pj.GenerateJson();
    }
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zonesec.geoserver.make.json;

/**
 *
 * @author Thomas Dimakopoulos
 */
public class generatejson {

    public String MakeHeader() {
        String header = "{\n"
                + "\"type\": \"FeatureCollection\",\n"
                + "    \"stationId\": \"urn:x-zonesec:def:station:attikesdiadromes::sensors\",\n"
                + "    \"features\": [";
        return header;
    }

    public String MakeEnd() {
        String end = "]\n"
                + "}";
        return end;
    }

    public String AddAnother() {
        return ",";
    }

//    JSON Element example takens from NOAA Sample dataset
//    {"type": "Feature",
//            "geometry": {
//                "type": "MultiPoint",
//                "coordinates": [[-78.48,33.83,3],[-78.48,33.83,3],[-78.48,33.83,3],[-78.48,33.83,3],[-78.48,33.83,3],[-78.48,33.83,3],[-78.48,33.83,3],[-78.48,33.83,3],[-78.48,33.83,3],[-78.48,33.83,3],[-78.48,33.83,3],[-78.48,33.83,3],[-78.48,33.83,3],[-78.48,33.83,3],[-78.48,33.83,3],[-78.48,33.83,3],[-78.48,33.83,3],[-78.48,33.83,3],[-78.48,33.83,3],[-78.48,33.83,3],[-78.48,33.83,3],[-78.48,33.83,3],[-78.48,33.83,3],[-78.48,33.83,3],[-78.48,33.83,3],[-78.48,33.83,3],[-78.48,33.83,3],[-78.48,33.83,3]] 
//            },
//         "properties": {
//            "obsTypeDesc": "air pressure",
//            "obsType": "air_pressure",
//            "uomType": "mb",
//            "sorder": "1",
//            "time": ["2016-11-21 13:08:00Z","2016-11-21 14:08:00Z","2016-11-21 15:08:00Z","2016-11-21 16:08:00Z","2016-11-21 17:08:00Z","2016-11-21 18:08:00Z","2016-11-21 19:08:00Z","2016-11-21 20:08:00Z","2016-11-21 21:08:00Z","2016-11-21 22:08:00Z","2016-11-21 23:08:00Z","2016-11-22 00:08:00Z","2016-11-22 01:08:00Z","2016-11-22 02:08:00Z","2016-11-22 03:08:00Z","2016-11-22 04:08:00Z","2016-11-22 05:08:00Z","2016-11-22 06:08:00Z","2016-11-22 07:08:00Z","2016-11-22 08:08:00Z","2016-11-22 09:08:00Z","2016-11-22 10:08:00Z","2016-11-22 11:08:00Z","2016-11-22 12:08:00Z","2016-11-22 13:08:00Z","2016-11-22 14:08:00Z","2016-11-22 15:08:00Z","2016-11-22 16:08:00Z"],
//            "value": ["1019.7","1020.2","1019.8","1019.2","1018.3","1017.7","1016.6","1016.4","1016.9","1017.8","1018.5","1019.3","1019.7","1020.3","1020.3","1020.5","1020.7","1020.5","1020.9","1021.5","1021.8","1022.8","1023.7","1024.8","1025.2","1025.6","1025.8","1025.4"]
//        }}
    
    public String AddElement(String sensorid,String lat, String log, String elev, String type, String desc, String uomtype, String time, String value) {
        //we use multipoint so can so a big circle and not a point
        String Element = "{\"type\": \"Feature\",\n"
                + "            \"geometry\": {\n"
                + "                \"type\": \"MultiPoint\",\n"
                + "                \"coordinates\": [[" + lat + "," + log + "," + elev + "],[" + lat + "," + log + "," + elev + "]] \n"
                + "            },\n"
                + "         \"properties\": {\n"
                + "            \"sensorID\": \"" + sensorid + "\",\n"
                + "            \"obsTypeDesc\": \"" + desc + "\",\n"
                + "            \"obsType\": \" " + type + "\",\n"
                + "            \"uomType\": \"" + uomtype + "\",\n"
                + "            \"time\": [\"" + time + "\"],\n"
                + "            \"value\": [\"" + value + "\"]\n"
                + "        }}";
        return Element;
    }
}

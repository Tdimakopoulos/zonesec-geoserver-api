/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zonesec.geoserver.api;

import org.zonesec.geoserver.dbmem.dbmem;
import org.zonesec.geoserver.dbmem.geoinformation;
import org.zonesec.geoserver.dbmem.measurment;
import org.zonesec.geoserver.dbmem.position;
import org.zonesec.geoserver.dbmem.sensor;
import org.zonesec.geoserver.make.json.generatejson;

/**
 *
 * @author Thomas Dimakopoulos
 */
public class GenerateSensorGeoJson {

    dbmem pmem = new dbmem();

    private geoinformation GetGeoInfoForID(int id) {
        for (int i = 0; i < pmem.getPgeoinfo().size(); i++) {
            if (pmem.getPgeoinfo().get(i).getGeoid() == id) {
                return pmem.getPgeoinfo().get(i);
            }
        }
        return null;
    }

    private sensor GetSensorForID(int id) {
        for (int i = 0; i < pmem.getPsensors().size(); i++) {
            if (pmem.getPsensors().get(i).getSensor_id() == id) {
                return pmem.getPsensors().get(i);
            }
        }
        return null;
    }

    private position GetPositionForID(int id) {
        for (int i = 0; i < pmem.getPpositions().size(); i++) {
            if (pmem.getPpositions().get(i).getPosid() == id) {
                return pmem.getPpositions().get(i);
            }
        }
        return null;
    }

    private measurment GetMeasurmentForID(int id) {
        for (int i = 0; i < pmem.getPmeasurments().size(); i++) {
            if (pmem.getPmeasurments().get(i).getMid() == id) {
                return pmem.getPmeasurments().get(i);
            }
        }
        return null;
    }

    public String GenerateJson() {
        String JsonReturn = "";
        generatejson pjson = new generatejson();
        JsonReturn = JsonReturn + pjson.MakeHeader();
        pmem.Connect();
        pmem.Loadgeoinformation();
        pmem.Loadmeasurments();
        pmem.Loadobservation();
        pmem.Loadposition();
        pmem.Loadsensor();
        pmem.CloseAll();
        for (int i = 0; i < pmem.getPobservations().size(); i++) {
            int geoID = pmem.getPobservations().get(i).getGeoid();
            geoinformation geoinfo = GetGeoInfoForID(geoID);//lot,lag,ele

            int posID = pmem.getPobservations().get(i).getPosid();
            position positioninfo = GetPositionForID(posID);//mesid
            measurment mesinfo = GetMeasurmentForID(positioninfo.getMesid()); // uom and value

            int sensorID = pmem.getPobservations().get(i).getSensorid_id();
            sensor sensorinfo = GetSensorForID(sensorID);//type and timestamp
            String lat = String.valueOf(geoinfo.getLatitudemid());
            String log = String.valueOf(geoinfo.getLongitudemid());
            String ele = String.valueOf(geoinfo.getElevationmid());
            String type = sensorinfo.getSensortype();
            String uom = String.valueOf(mesinfo.getUom());
            String time = sensorinfo.getTimestamp();
            String value = String.valueOf(mesinfo.getValue());
            JsonReturn = JsonReturn + pjson.AddElement(String.valueOf(sensorID),lat, log, ele, type, type, uom, time, value);
            JsonReturn = JsonReturn + pjson.AddAnother();
        }
        JsonReturn = JsonReturn + pjson.MakeEnd();
        return JsonReturn;
    }
}

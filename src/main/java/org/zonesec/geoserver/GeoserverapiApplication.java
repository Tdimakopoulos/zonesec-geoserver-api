package org.zonesec.geoserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GeoserverapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(GeoserverapiApplication.class, args);
	}
}

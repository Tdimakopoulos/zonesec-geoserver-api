/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zonesec.geoserver.db.insert;

import java.io.Serializable;
import java.util.Date;
import org.zonesec.geoserver.db.connectionmanager.connectionmanager;
import org.zonesec.geoserver.db.statementmanager.statementmanager;
import org.zonesec.geoserver.dbmem.dbmem;

/**
 *
 * @author Thomas Dimakopoulos
 */
public class InsertManager implements Serializable {

    

    public void AddSensor(int sensorid,String sensortype,int timestamp,int sensor_id) {
        String psql = "INSERT INTO \"public\".\"sensor\" (sensorid,sensortype,timestamp,sensorid_id) VALUES ("+sensorid+",'"+sensortype+"','"+timestamp+"','"+sensor_id+"');";
        connectionmanager pp = new connectionmanager();

        pp.ConnectTODB();
        statementmanager psm = new statementmanager();
        psm.CreateStatementi(pp.getC());
        psm.ExecuteUpdate(psql);
        psm.CloseStatement();
        pp.CloseDB();
    }

    public void AddGeoInformation(int latitudemid,int longitudemid,int orientationmid,int elevationmid,int geoid) {
        String psql = "INSERT INTO \"public\".\"geoinformation\" (latitudemid,longitudemid,orientationmid,elevationmid,geoid) VALUES ("+latitudemid+","+longitudemid+","+orientationmid+","
                + elevationmid+","+geoid+");";
        connectionmanager pp = new connectionmanager();

        pp.ConnectTODB();
        statementmanager psm = new statementmanager();
        psm.CreateStatementi(pp.getC());
        psm.ExecuteUpdate(psql);
        psm.CloseStatement();
        pp.CloseDB();
    }

    
    public void AddGeoInformation2(int latitudemid,int longitudemid,int orientationmid,int elevationmid,int geoid) {
        String psql = "INSERT INTO \"public\".\"geoinformation\" (latitudemid,longitudemid,orientationmid,elevationmid,geoid) VALUES ("+latitudemid+","+longitudemid+","+orientationmid+","
                + elevationmid+","+geoid+");";
        connectionmanager pp = new connectionmanager();

        pp.ConnectTODB();
        statementmanager psm = new statementmanager();
        psm.CreateStatementi(pp.getC());
        psm.ExecuteUpdate(psql);
        psm.CloseStatement();
        pp.CloseDB();
    }
}
